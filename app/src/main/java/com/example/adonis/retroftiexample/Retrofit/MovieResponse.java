package com.example.adonis.retroftiexample.Retrofit;

import com.example.adonis.retroftiexample.Movie;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Adonis on 7/13/2017.
 */

public class MovieResponse {

    @SerializedName("page")
    private int page;

    @SerializedName("total_results")
    private int totalResults;

    @SerializedName("total_pages")
    private int totalPages;

    @SerializedName("results")
    private List<Movie> movies;

    public int getPage() {
        return page;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public List<Movie> getResults() {
        return movies;
    }
}
