package com.example.adonis.retroftiexample.Retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Adonis on 7/13/2017.
 */

public interface API {


    @GET("discover/movFie?page=1&api_key=1906833e4f7429ebcd27cb7a04da7c0a")
    Call<MovieResponse> getMovies();

    @GET("discover/movie")
    Call<MovieResponse> getMovies(@Query("page") int page,
                                  @Query("api_key") String apiKey);


}
