package com.example.adonis.retroftiexample.RecyclerView;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.adonis.retroftiexample.Movie;
import com.example.adonis.retroftiexample.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Adonis on 7/13/2017.
 */

public class CustomAdapter extends RecyclerView.Adapter<CustomViewHolder> {


    private List<Movie> movieList;
    private Context context;

    public CustomAdapter(List<Movie> movieList) {
        this.movieList = movieList;
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        return new CustomViewHolder(v);
    }

    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        Movie movie = movieList.get(position);
        Picasso.with(context)
                .load(movie.getPosterUrl())
                .fit()
                .centerInside()
                .into(holder.ivPoster);
        holder.tvTitle.setText(movie.getTitle());
        holder.tvDescription.setText(movie.getDescription());
    }

    @Override
    public int getItemCount() {
        return movieList != null ? movieList.size() : 0;
    }
}
