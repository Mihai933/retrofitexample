package com.example.adonis.retroftiexample.RecyclerView;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.adonis.retroftiexample.R;

/**
 * Created by Adonis on 7/13/2017.
 */

public class CustomViewHolder extends RecyclerView.ViewHolder {

    TextView tvTitle, tvDescription;
    ImageView ivPoster;

    public CustomViewHolder(View itemView) {
        super(itemView);
        tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
        tvDescription = (TextView) itemView.findViewById(R.id.tv_description);
        ivPoster = (ImageView) itemView.findViewById(R.id.iv_poster);
    }
}
