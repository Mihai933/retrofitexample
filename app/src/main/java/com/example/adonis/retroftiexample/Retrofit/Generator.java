package com.example.adonis.retroftiexample.Retrofit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Adonis on 7/13/2017.
 */

public class Generator {

    private static final String baseURL = "https://api.themoviedb.org/3/";
    private static Retrofit retrofit;


    public static <S> S createService(Class<S> serviceClass) {
        return getRetrofit().create(serviceClass);
    }

    private static Retrofit getRetrofit() {
        if (retrofit == null) {

            OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();

            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            okHttpBuilder.addInterceptor(loggingInterceptor);

            Retrofit.Builder builder =
                    new Retrofit.Builder()
                            .baseUrl(baseURL)
                            .addConverterFactory(GsonConverterFactory.create())
                            .client(okHttpBuilder.build());
            retrofit = builder.build();
        }
        return retrofit;
    }
}
