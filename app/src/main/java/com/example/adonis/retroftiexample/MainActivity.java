package com.example.adonis.retroftiexample;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.adonis.retroftiexample.RecyclerView.CustomAdapter;
import com.example.adonis.retroftiexample.Retrofit.API;
import com.example.adonis.retroftiexample.Retrofit.ApiPeople;
import com.example.adonis.retroftiexample.Retrofit.Generator;
import com.example.adonis.retroftiexample.Retrofit.MovieResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    RecyclerView recyclerView;
    CustomAdapter adapter;
    API apiClient;
    ApiPeople apiPeople;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        getApiPeople(68, "de");
    }


    private void initializeRecyclerView(List<Movie> movieList) {
        adapter = new CustomAdapter(movieList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    private void getApiPeople(int i, String de) {
        showProgressDialog();

        if (apiPeople == null) {
            apiPeople = Generator.createService(ApiPeople.class);
        }

        apiPeople.getMoviesFormApi2(i, de)
                .enqueue(new Callback<MovieResponse>() {
                    @Override
                    public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()) {
                            MovieResponse movieResponse = response.body();
                            if (movieResponse != null) {
                                initializeRecyclerView(movieResponse.getResults());
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "Response is not succesfull", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<MovieResponse> call, Throwable t) {
                        hideProgressDialog();
                        Toast.makeText(MainActivity.this, "Failed: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void getMovies() {
        showProgressDialog();

        apiClient = Generator.createService(API.class);

        apiClient.getMovies()
                .enqueue(new Callback<MovieResponse>() {
                    @Override
                    public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()) {
                            MovieResponse movieResponse = response.body();
                            if (movieResponse != null) {
                                initializeRecyclerView(movieResponse.getResults());
                            }
                        } else {
                            Toast.makeText(MainActivity.this, "Response is not succesfull", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<MovieResponse> call, Throwable t) {
                        hideProgressDialog();
                        Toast.makeText(MainActivity.this, "Failed: " + t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });

        apiClient.getMovies().enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {

            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {

            }
        });
    }

    private void showProgressDialog() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Fetching data...");
        }
        progressDialog.show();
    }

    private void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private void getMovies(int page, String apiKey) {
        showProgressDialog();

        apiClient = Generator.createService(API.class);
        apiClient.getMovies(page, apiKey).enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    MovieResponse movieResponse = response.body();
                    if (movieResponse != null) {
                        initializeRecyclerView(movieResponse.getResults());
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Response is not succesfull", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {
                hideProgressDialog();
                Toast.makeText(MainActivity.this, "Failed: " + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

}
