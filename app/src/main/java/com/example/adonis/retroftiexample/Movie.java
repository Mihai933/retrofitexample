package com.example.adonis.retroftiexample;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Adonis on 7/13/2017.
 */

public class Movie {

    @SerializedName("title")
    private String title;

    @SerializedName("overview")
    private String description;

    @SerializedName("poster_path")
    private String posterUrl;

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getPosterUrl() {
        return "https://image.tmdb.org/t/p/w500/" + posterUrl;
    }
}
