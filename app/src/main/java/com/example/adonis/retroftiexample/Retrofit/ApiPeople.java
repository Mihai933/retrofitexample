package com.example.adonis.retroftiexample.Retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Adonis on 7/13/2017.
 */

public interface ApiPeople {


    @GET("discover/movie?api_key=1906833e4f7429ebcd27cb7a04da7c0a")
    Call<MovieResponse> getMoviesFormApi2(@Query("page") int page,
                                          @Query("language") String language);
}
